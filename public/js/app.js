(function (w, factory){

	factory(w);
})(window, function (w){
	'use strict';

//////////////////////////////////////////////////
	function isArray(o){
		return Array.isArray(o);
	}

	function isObject(o){
		return typeof o === 'object';
	}

	function isFunction(o){
		return typeof o === 'function';
	}

	function mergeObj(obj1, obj2){

		var o = {};
		for(var i in obj1){
			if(obj1.hasOwnProperty(i))
				o[i] = obj1[i];
		}
		for(var i in obj2){
			if(obj2.hasOwnProperty(i))
				o[i] = obj2[i];
		}
		console.log(o);
		return o;
	}

	function equal(obj1, obj2){
		return JSON.stringify(obj1) === JSON.stringify(obj2);
	}

	function forEach(array, callback){
		for(var i = 0; i < array.length; i++)
			callback(array[i], i);
	}

//////////////////////////////////////////////////

	Object.prototype.clone = function (){

		return JSON.parse(JSON.stringify(this));
	};

//////////////////////////////////////////////////

	function Scope(){

		var _watchers = [];
		var self = this;
		this.$watch = function (watcher, atChange){

			_watchers.push({
				prop: watcher(),
				atChange: atChange || function (){}
			});
		};

		this.$diggest = function (watcher){

			forEach(_watchers, function (item, i){
				if(equal(item.prop, watcher()))
					_diggest(i);
			});
		};

		function _diggest(index){

			var watcher = _watchers[index];
			if(!equal(self[watcher.prop], watcher.last)){
				watcher.atChange(self[watcher.prop], watcher.last);
				watcher.last = self[watcher.prop];
			}
		}
	}
//////////////////////////////////////////////////

	function Directive(){

	}
//////////////////////////////////////////////////

	HTMLCompiler();
	function HTMLCompiler(){

		seekForDirectives();
		function seekForDirectives(){

			var directives = [];
		}
		function driveNodes(parent){


		}
	}
//////////////////////////////////////////////////

	function executer(){

	}
//////////////////////////////////////////////////

	var $http = w.$http = {};
	var fn = $http.__proto__,
		success = function (f){
			this.suc = f;
			return erSucc(this);
		},
		error = function (f){
			this.err = f;
			return erSucc(this);
		},
		notFound = function (f){
			this.not = f;
			return erSucc(this);
		},
		seekFor = function (obj){
			for(var o in obj){
				switch(o){
					case 'console':
						console.log(obj[o]);
						break;
					case 'goto':
						w.location.href = obj[o];
						break;
					case 'delLS':
						delete w.localStorage[obj[o]];
						break;
					case 'setLS':
						for(var i in obj[o])
							w.localStorage[i] = obj[o][i];
						break;
					case 'html':

						break;
				}
			}
		};
	XMLHttpRequest.prototype.error = error;
	XMLHttpRequest.prototype.success = success;
	XMLHttpRequest.prototype.not = notFound;
	XMLHttpRequest.prototype.suc = function (){};
	XMLHttpRequest.prototype.err = function (){};
	XMLHttpRequest.prototype.not = function (){};
	var erSucc = function (from){
		from.onreadystatechange = function() {
			if(from.readyState == 4) {
				if(from.status == 200) {
					from.suc(from.responseText, from.status);
				}
				else if(from.status == 220){// Status for JSON string
					var o = JSON.parse(from.responseText);
					from.suc(o, from.status);
					seekFor(o);
				}
				else if(from.status == 222){// Status for JSON error
					var o = JSON.parse(from.responseText);
					from.err(o, from.status);
					seekFor(o);
				}
				else if(from.status == 404){
					from.not(from.responseText, from.status);
				}
			}
		};
		return from;
	};
	fn.get = function (url, asy){
		var xhttp = new XMLHttpRequest();
		xhttp.open('get', encodeURI(url), asy);
		xhttp.send();
		return xhttp;
	};
	fn.post = function (url, data, asy){
		var xhttp = new XMLHttpRequest();
		xhttp.open('post', encodeURI(url), asy);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(encodeURI(data));
		return xhttp;
	};
	fn.head = function (url, asy){
		var xhttp = new XMLHttpRequest();
		xhttp.open('head', url, asy);
		xhttp.send();
		return xhttp;
	};
	fn.put = function (url, data, asy){
		var xhttp = new XMLHttpRequest();
		xhttp.open('put', url, asy);
		xhttp.setRequestHeader('Content-Type', 'application/json');
		xhttp.send(JSON.stringify(data));
		return xhttp;
	};
	fn.delete = function (url, asy){
		var xhttp = new XMLHttpRequest();
		xhttp.open('delete', encodeURI(url), asy);
		xhttp.send();
		return xhttp;
	};

//////////////////////////////////////////////////

});