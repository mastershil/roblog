/**
 * Created by germanamz on 2/18/16.
 */
(function (w, factory){

	var gW = document.querySelector('#globalWrapper');
	if(gW.getAttribute('seted') === 'true')
		return ;
	factory(w, gW);
})(window, function (w, globalWrapper){

});